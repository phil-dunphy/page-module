<?php
namespace aca\page\common\models;

use Yii;
use yii\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class PageAccessLevel extends \yii\db\ActiveQuery
{

    public function getPagesId()
    {
        $userAccessedPages = $this->getUserAccessedPages();
        if (!empty($userAccessedPages)) {
            return $userAccessedPages;
        }
        return $this->getNonAccessSetPages();
    }

    private function getNonAccessSetPages()
    {
        return Yii::$app->db->createCommand(
            'SELECT  `id`
            from `page`
            where `id` not in
                (SELECT distinct `page`.`id`
                FROM `page_user`
                INNER JOIN `page` ON `page_user`.`pageId` = `page`.`root`
                )
            '
        )
        ->queryColumn();
    }

    private function getUserAccessedPages()
    {
        return Yii::$app->db->createCommand(
            'SELECT distinct `page`.`id`
            FROM `page_user`
            INNER JOIN `page` ON `page_user`.`pageId` = `page`.`root`
            WHERE userId=:id'
        )->bindValue(':id', Yii::$app->user->id)
        ->queryColumn();
    }
}
