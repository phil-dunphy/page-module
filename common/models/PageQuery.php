<?php
namespace aca\page\common\models;

use Yii;
use yii\helpers\ArrayHelper;
use creocoder\nestedsets\NestedSetsQueryBehavior;

class PageQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function checkAccessForUser()
    {
        if (Yii::$app->user->isSuperuser()) {
            return $this;
        }
        $pageAccessLevel = new PageAccessLevel(get_called_class());
        return $this->andWhere(
            ['in', 'id', $pageAccessLevel->getPagesId()]
        );
    }
}
