<?php
namespace aca\page\common\models;

use Yii;
use yii\helpers\ArrayHelper;
use aca\gallery\behaviors\GalleryBehavior;
use creocoder\nestedsets\NestedSetsBehavior;

class Page extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'page';
    }

    public function behaviors()
    {
        return [
            GalleryBehavior::className(),
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'root',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth',
            ],
        ];
    }

    public function getTree()
    {
        $categoryIds =  ArrayHelper::getColumn(
            $this->children()->select('id')->asArray()->all(),
            'id'
        );

        $categoryIds[] = $this->id;
        return $categoryIds;
    }
}
