<?php

use yii\db\Migration;

class m171118_093627_add_layout_to_page extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'layout', 'smallint DEFAULT 2'.' COMMENT "1=>one-column, 2=>two-column"');

    }

    public function down()
    {
        echo "m171118_093627_add_layout_to_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
