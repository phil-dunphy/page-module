<?php

use yii\db\Migration;
use aca\setting\backend\models\Setting;
use aca\website\backend\models\Website;

class m170215_134535_add_setting_and_change_confirm_status_of_page extends Migration
{
    public function safeUp()
    {
        $this->insert('setting', [
            'key' => 'page.editor',
            'value' => 1,
            'websiteId' => Website::getDefaultWebsiteAttr('id')
        ]);
        $editorIsEnabled = Setting::find()->select('value')->where(['key' => 'website.editorIsEnabled'])->one();
        if (isset($editorIsEnabled->value) and !$editorIsEnabled->value) {
            $this->update('page', ['confirmStatus' => 2]);
        }
    }

    public function safeDown()
    {
        $this->delete('setting', ['key' => 'page.editor']);
    }
}

