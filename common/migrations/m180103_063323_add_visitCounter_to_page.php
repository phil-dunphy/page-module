<?php

use yii\db\Migration;
use aca\website\backend\models\Website;

class m180103_063323_add_visitCounter_to_page extends Migration
{
    public function safeUp()
    {
        $this->addColumn('page', 'visitCounter', 'int');

        $this->insert('setting', [
            'key' => 'page.showVisitStatistic',
            'value' => 0,
            'websiteId' => Website::getDefaultWebsiteAttr('id')
        ]);
    }

    public function safeDown()
    {
        echo "m180103_063323_add_visitCounter_to_page_table cannot be reverted.\n";
        return false;
    }
}
