<?php

use yii\db\Migration;

class m160413_123659_add_index_to_page extends Migration
{
    public function up()
    {
        $this->createIndex('page_websiteIdIndex', 'page', 'websiteId');
        $this->createIndex('page_confirmStatusIndex', 'page', 'confirmStatus');
        $this->createIndex('page_activeIndex', 'page', 'isActive');
    }
}
