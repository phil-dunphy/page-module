<?php

use yii\db\Migration;
use aca\website\backend\models\Website;
/**
 * Handles adding lastUpdatedAt to table `setting`.
 */
class m170417_093933_add_lastUpdatedAt_column_to_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->insert('setting', [
            'key' => 'page.lastUpdatedAt',
            'value' => 0,
            'websiteId' => Website::getDefaultWebsite()->id
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
