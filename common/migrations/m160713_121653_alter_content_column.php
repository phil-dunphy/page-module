<?php

use yii\db\Migration;

class m160713_121653_alter_content_column extends Migration
{
    public function up()
    {
        $this->alterColumn('page', 'content', 'longtext COLLATE utf8_unicode_ci NOT NULL');
    }

    public function down()
    {
        echo "m160713_121653_alter_content_column cannot be reverted.\n";

        return false;
    }

}
