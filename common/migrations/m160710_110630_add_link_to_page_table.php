<?php

use yii\db\Migration;

class m160710_110630_add_link_to_page_table extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'link', 'text COLLATE utf8_unicode_ci');
    }

    public function down()
    {
        $this->dropColumn('page', 'link');
    }
}
