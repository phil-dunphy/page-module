<?php

use yii\db\Migration;

class m160608_091327_remove_slug_field extends Migration
{
    public function up()
    {
            $table = Yii::$app->db->schema->getTableSchema('page');
            if(isset($table->columns['slug'])) {
                $this->dropIndex('idx-page_slug', 'page');
                $this->dropColumn('page', 'slug');
            }

    }

    public function down()
    {
        echo "m160608_091327_remove_slug_field cannot be reverted.\n";

        return false;
    }
}
