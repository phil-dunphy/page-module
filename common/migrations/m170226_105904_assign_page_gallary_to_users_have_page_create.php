<?php

use yii\db\Migration;

class m170226_105904_assign_page_gallary_to_users_have_page_create extends Migration
{
    public function up()
    {
        $usersId = Yii::$app->authManager->getUserIdsByRole('page.create');
        $galleryPermission = Yii::$app->authManager->getPermission('page.gallery');
        foreach ($usersId as $userId) {
            if (!Yii::$app->authManager->checkAccess($userId, $galleryPermission->name)) {
                Yii::$app->authManager->assign($galleryPermission, $userId);
            }
        }
    }

    public function down()
    {
        echo "m170226_105904_assign_page_gallary_to_users_have_page_create cannot be reverted.\n";
        return false;
    }
}