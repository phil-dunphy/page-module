<?php

use yii\db\Migration;
use aca\website\backend\models\Website;

class m190408_095240_add_access_level_to_page extends Migration
{
    public function safeUp()
    {
        $this->insert('setting', [
            'key' => 'page.hasAccessLevel',
            'value' => $this->usedThisFeature(),
            'websiteId' => Website::getDefaultWebsite()->id
        ]);
    }

    private function usedThisFeature()
    {
        $hasTeachersInfo = Yii::$app->db->createCommand(
            '
            SELECT count(*) FROM `module`
            WHERE  `moduleId` LIKE  "teachersInfo"
            '
        )->queryScalar();
        return (Yii::$app->setting->get('website.isEpage') || !empty($hasTeachersInfo))? 0 : 1 ;
    }

    public function safeDown()
    {
        echo "m190408_095240_add_access_level_to_page cannot be reverted.\n";

        return false;
    }
}
