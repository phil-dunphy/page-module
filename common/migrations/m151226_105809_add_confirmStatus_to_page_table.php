<?php

use yii\db\Schema;
use yii\db\Migration;

class m151226_105809_add_confirmStatus_to_page_table extends Migration
{
    public function up()
    {
        $this->addColumn('page','confirmStatus','smallint NOT NULL DEFAULT 2'.
            ' COMMENT "0=>rejected, 1=>waiting, 2=>accepted"');
    }

    public function down()
    {
        echo "m151226_105809_add_confirmStatus_to_page_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
