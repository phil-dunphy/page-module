<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page_user`.
 */
class m180729_063341_create_page_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('page_user', [
             'userId' => $this->integer()->notNull(),
             'pageId' => $this->integer()->notNull()
            ], $tableOptions
        );

        $this->addPrimaryKey('PK_page_user', 'page_user', 'userId, pageId');
        $this->addForeignKey('FK_page_user', 'page_user', 'userId', 'user', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_page', 'page_user', 'pageId', 'page', 'id', 'CASCADE', 'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('page_user');
    }
}
