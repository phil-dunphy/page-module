<?php
use aca\page\backend\Module;

return [
    'permissions' => [
       'create'  => Module::t('Create'),
       'update'  => Module::t('Update'),
       'delete'  => Module::t('Delete'),
       'setting' => Module::t('Setting'),
       'gallery' => Module::t('Gallery')
    ],
    'roles' => [

    ],
    'relations' => [

    ],
    'ruleAssignments' => [

    ],
];
