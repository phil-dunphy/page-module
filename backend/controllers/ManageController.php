<?php

namespace aca\page\backend\controllers;

use Yii;
use aca\page\backend\Module;
use yii\filters\AccessControl;
use aca\page\backend\models\Page;
use aca\page\backend\models\PageSearch;
use aca\backend\classes\AdminController;

class ManageController extends AdminController
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['update'],
                            'roles' => ['page.update'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['delete'],
                            'roles' => ['page.delete'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['create'],
                            'roles' => ['page.create'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['setting'],
                            'roles' => ['page.setting'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['gallery'],
                            'roles' => ['page.gallery'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['index', 'view'],
                            'roles' => ['page.create', 'page.update', 'page.delete', 'page.gallery'],
                        ],
                        [
                            'allow' => true,
                            'actions' => ['accept','reject'],
                            'roles' => ['editor']
                        ],
                    ],
                ],
            ]
        );
    }

    public function actions()
    {
        return [
            'gallery' => [
                'class' => 'aca\gallery\actions\GalleryAction',
                'ownerModelClassName' => Page::className()
            ],
            'setting' => [
                'class' => 'aca\setting\backend\actions\ModuleSettingAction',
                'moduleName' => 'page',
            ]
        ];
    }

    public function init()
    {
        parent::init();
        $this->modelClass = Page::className();
        $this->searchClass = PageSearch::className();
    }

    public function actionCreate()
    {
        $model = new Page();

        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post())) {
            $model->root = $_POST['Page']['parentId'];
            if ($_POST['Page']['parentId'] != 0) {
                $parent =  Page::findOne(['id' => $_POST['Page']['parentId']]);
                $success = $model->appendTo($parent);
            } else {
                $success = $model->makeRoot();
            }
            if ($success) {
                Yii::$app->session->addFlash('success', Module::t('Data inserted successfully.'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $page = $this->findModel($id);
        $parent = $page->parents(1)->one();
        $children = $page->children()->all();
        return $this->render('view', [
            'model' => $page,
            'parent' => $parent,
            'children' => $children,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($_POST['Page']['parentId'] != '0') {
                $parent = $model->parents(1)->one();
                if (!isset($parent) or $parent->id != $_POST['Page']['parentId']) {
                    $root = Page::findOne(['id' => $_POST['Page']['parentId']]);
                    $success = $model->appendTo($root);
                } else {
                    $success = $model->save();
                }
            } else {
                if ($model->isRoot()) {
                    $success = $model->save();
                } else {
                    $success = $model->makeRoot();
                }
            }

            if ($success) {
                Yii::$app->session->addFlash(
                    'success',
                    Yii::t('core', 'Data edited successfully.')
                );
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        $deletion = $model->deleteWithChildren();
        Yii::$app->session->addFlash(
            ($deletion) ? 'success' : 'danger',
            ($deletion) ? Yii::t('core', 'Data deleted successfully') : $model->getFirstError('id')
        );
        return $this->redirect(['index']);
    }
}
