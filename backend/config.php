<?php

use aca\page\backend\Module;

return [
    'title' => Module::t('Pages'),
    'menu' => [
        'label' => Module::t('Pages'),
        'icon' => 'file',
        'items' => [
            [
                'label' => Module::t('Add New Page'),
                'url' => ['/page/manage/create'],
                'visible' => Yii::$app->user->can('page.create')
            ],
            [
                'label' => Module::t('Page List'),
                'url' => ['/page/manage/index'],
                'visible' => Yii::$app->user->canAccessAny([
                    'page.create',
                    'page.update',
                    'page.delete',
                    'page.gallery'
                ])
            ],
            [
                'label' => Yii::t('core', 'Settings'),
                'url' => ['/page/manage/setting'],
                'visible' =>  Yii::$app->user->can('page.setting')
            ],
        ],
    ],
    'components' => [
    ],
    'params' => [
    ],
];
