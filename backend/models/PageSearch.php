<?php

namespace aca\page\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use aca\page\backend\models\Page;

class PageSearch extends Page
{

    public function rules()
    {
        return [
            [['id', 'isActive', 'root', 'lft', 'confirmStatus', 'layout'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Page::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'root' => SORT_DESC,
                    'lft' => SORT_ASC,
                ]
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'isActive' => $this->isActive,
            'root' => $this->root,
            'lft' => $this->lft,
            'confirmStatus' => $this->confirmStatus,
            'layout' => $this->layout
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
