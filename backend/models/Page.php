<?php
namespace aca\page\backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use aca\page\backend\Module;
use aca\common\helpers\Utility;
use aca\user\backend\models\User;
use aca\backend\traits\EditorTrait;
use aca\page\common\models\PageQuery;
use aca\visit\behaviors\VisitBehavior;
use aca\common\behaviors\TimestampBehavior;
use aca\fileManager\behaviors\FileBehavior;
use aca\common\behaviors\OwnerUsersBehavior;
use aca\page\common\models\Page as basePage;
use aca\tag\common\behaviors\TaggableBehavior;
use aca\backend\behaviors\cache\CacheBehavior;
use aca\log\backend\behaviors\LoggableBehavior;
use aca\backend\behaviors\editor\EditorBehavior;
use aca\common\validators\ConvertToFarsiNumbers;
use aca\common\validators\ConvertFarsiCharacters;
use aca\common\behaviors\website\WebsitesBehavior;
use aca\backend\behaviors\previewLink\PreviewLink;

class Page extends basePage
{
    use EditorTrait;
    public $keywords;
    private $parentId;

    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                TimestampBehavior::className(),
                LoggableBehavior::className(),
                OwnerUsersBehavior::className(),
                CacheBehavior::className(),
                [
                    'class' => EditorBehavior::className(),
                    'moduleId' => 'page'
                ],
                WebsitesBehavior::className(),
                [
                    'class' => FileBehavior::className(),
                    'groups' => [
                        'image' => [
                            'type' => FileBehavior::TYPE_IMAGE,
                            'rules' => [
                                'extensions' => ['png', 'jpg', 'jpeg', 'gif']
                            ]
                        ],
                        'file' => [
                            'type' => FileBehavior::TYPE_FILE,
                            'rules' => [
                                'extensions' => ['mp4', 'ogg', 'webm', 'pdf', 'zip','doc', 'docx', 'rar', 'mp3']
                            ]
                        ]
                    ]
                ],
                [
                    'class' => TaggableBehavior::className(),
                    'moduleId' => Yii::$app->controller->module->id,
                ],
                [
                    'class' => PreviewLink::className(),
                    'viewUrl' => 'page/front/view',
                    'previewUrl' => 'page/front/preview'
                ],
                VisitBehavior::className()
            ]
        );
    }

    public function rules()
    {
        return [
            [['title', 'parentId'], 'required'],
            ['title', 'trim'],
            ['link', 'url'],
            [['content'], 'string'],
            [['tags'], 'safe'],
            [['createdAt', 'updatedAt', 'isActive', 'root', 'lft', 'rgt', 'depth', 'websiteId','priority', 'layout'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['users'], 'safe'],
            [['title', 'content', 'tags'], ConvertFarsiCharacters::className()],
            [['title'], ConvertToFarsiNumbers::className()]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('core', 'ID'),
            'title' => Yii::t('core', 'Title'),
            'isActive' => Yii::t('core', 'Is Active'),
            'content' => Module::t('Content'),
            'parentId' => Module::t('Parent'),
            'createdAt' => Yii::t('core', 'Created At'),
            'updatedAt' => Yii::t('core', 'Updated At'),
            'priority' => Yii::t('core', 'Priority'),
            'link' => Module::t('Link'),
            'confirmStatus' => Yii::t('core', 'Confirm Status'),
            'layout' => Yii::t('core', 'Layout'),
            'visitCounter' => Yii::t('core', 'Visit Count'),
            'users' => Module::t('Granting Access To Users'),
            'tags' => Yii::t('core', 'Tags'),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        $query = new PageQuery(get_called_class());
        $query->where(
            'websiteId = :id',
            [':id' => Yii::$app->website->id]
        );
        if (Yii::$app->setting->get('page.hasAccessLevel')) {
            $query->checkAccessForUser();
        }
        return $query;
    }

    public static function layoutLabel()
    {
        return [
            1 => Yii::t('core', 'One Column'),
            2 => Yii::t('core', 'Two Column'),
        ];
    }

    public function getLayoutLabel()
    {
        $types = $this->layoutLabel();
        return $types[$this->layout];
    }

    public function getNestedTitle()
    {
        $temp = '';
        for ($i = 0; $i < $this->depth; $i++) {
            $temp .= '- ';
        }

        return $temp.' '.$this->title;
    }

    public function getNestedList()
    {
        $family=[];
        if (!$this->isNewRecord) {
            $family[] = $this->id;
            $children = $this->children()->all();
            foreach ($children as $child) {
                    $family[] =  $child->id ;
            }
        }
        $page =  Utility::makeReadyForSelectize(
            Page::find()->andWhere(['not in','id',$family])->orderBy(['root' => SORT_DESC,'lft' => SORT_ASC])->all(),
            'id',
            'nestedTitle'
        );
        $first = [
            0 => [
                'id' => 0 ,
                'nestedTitle' => Module::t('Page is Root')
            ]
        ];
        $pages = array_merge($first, $page);

        return $pages ;
    }

    public function setParentId($value)
    {
        $this->parentId = $value;
    }

    public function getParentId()
    {
        if (!isset($this->parentId)) {
            $this->parentId = ($this->id == $this->root) ? 0 : $this->root;
            if ($this->id == $this->root) {
                $this->parentId = 0;
            } else {
                $this->parentId = $this->parents(1)->one()->id;
            }
        }

        return $this->parentId;
    }

    public function getNumberOfChildren()
    {
        return count($this->children()->all());
    }

    public function beforeDelete()
    {   if (parent::beforeDelete()) {
            if ($this->children()->all()) {
                $this->addError(
                    'id',
                    Module::t('Warning ! This page is a parent. To delete it, first delete the children.')
                );
                return false;
            }
            return true;
        }
        return false;

    }

    public function getUsersRelation()
    {
        return $this->hasMany(
            User::className(),
            ['id' => 'userId']
        )->viaTable('page_user', ['pageId' => 'id']);
    }

    public function canAccessToUsers()
    {
        if (!Yii::$app->setting->get('page.hasAccessLevel')) {
            return false;
        } elseif ($this->getParentId()) {
            return false;
        }
        return true;
    }
}
