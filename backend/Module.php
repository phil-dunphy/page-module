<?php

namespace aca\page\backend;

use yii\helpers\Url;
use aca\page\backend\models\Page;

class Module extends \yii\base\Module
{
    public $title;
    public $menu;
    public $urlRules;
    public $defaultRoute = 'manage/index';
    public $controllerNamespace = 'aca\page\backend\controllers';

    public function init()
    {
        parent::init();
        self::registerTranslations();
        $frontendConfigs = require dirname(__FILE__).'/../frontend/config.php';
        $this->urlRules = $frontendConfigs['urlRules'];
        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    public static function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/page/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@aca/page/common/messages',
            'fileMap' => [
                'modules/page/module' => 'module.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        if (!self::isTranslationRegistered()) {
            self::registerTranslations();
        }
        return \Yii::t('modules/page/module', $message, $params, $language);
    }

    private static function isTranslationRegistered()
    {
        return isset(\Yii::$app->i18n->translations['modules/page/*']);
    }

    public function feedMenuModule()
    {
        $pages = Page::find()
            ->andWhere('isActive = 1')
            ->orderBy(['root' => SORT_DESC,'lft' => SORT_ASC])
            ->all();
        $list[] = [
            'label' => self::t('Most Visited Pages'),
            'route' => '/page/front/popular',
            'params' => null
        ];
        foreach ($pages as $page) {
            $list[] = [
                'label' => $page->nestedTitle,
                'route' => 'page/front/view',
                'params' => [
                    'id' => $page->id,
                    'title' => $page->title
                ]
            ];
        }

        return $list;
    }

    public function editorConfirmationData()
    {
        return [
            "Page"=>[
                "count" => Page::find()
                    ->andWhere(['confirmStatus' => Page::$STATUS_WAITING])
                    ->count(),
                "url" =>Url::to(['/page/manage/index', 'PageSearch[confirmStatus]'=>Page::$STATUS_WAITING]),
                "title" =>self::t('Pages'),
                "visible" => \Yii::$app->user->canAccessAny([
                    'page.create',
                    'page.update',
                    'page.delete'
                ]),
                "ignoredActions" => ["gallery"]
            ]
        ];
    }

}
