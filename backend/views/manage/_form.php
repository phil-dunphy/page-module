<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use aca\common\helpers\Html;
use aca\page\backend\Module;
use aca\common\helpers\Utility;
use aca\backend\widgets\box\Box;
use aca\page\backend\models\Page;
use aca\backend\widgets\button\Button;
use aca\backend\widgets\ckeditor\Editor;
use aca\backend\widgets\priority\Priority;
use dosamigos\selectize\SelectizeTextInput;
use dosamigos\selectize\SelectizeDropDownList;
use aca\fileManager\widgets\uploader\SingleFileUpload;
use aca\fileManager\widgets\uploader\SingleImageUpload;
use aca\backend\widgets\activeWebsiteView\ActiveWebsiteView;

$backLink = $model->isNewRecord ? array('index') : array('view', 'id' => $model->id);

?>

<div class="page-form">
    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data'] // important
    ]); ?>
<div class="row">
    <div class="col-md-8">
    <?php Box::begin([
        'title' => Yii::t('core', 'Basic Information'),
        'options' => ['class' => 'box-solid box-primary'],
    ]) ?>
    <?= ActiveWebsiteView::widget(); ?>
    <?= $form->errorSummary($model, ['class'=>"text-danger"]); ?>
    <?= $form->field($model, 'title')
            ->textInput(['maxlength' => 255, 'class' => 'form-control input-large']) ?>
    <?= $form->field($model, 'parentId')->widget(SelectizeDropDownList::className(), [
            'options' => ['class' => 'form-control input-large'],
            'clientOptions' => [
                'options' => $model->getNestedList(),
                'items' => $model->isNewRecord ? [$model->getNestedList()[0]['id']]:[$model->parentId] ,
                'valueField' => 'id',
                'labelField' => 'nestedTitle',
                'searchField' => ['nestedTitle'],
            ],
    ]); ?>
    <?php if ($model->canAccessToUsers()): ?>
        <div id="accesslevel">
            <?= $form->field($model, 'users')->widget(
                SelectizeTextInput::className(), [
                    'options' => ['class' => 'form-control input-large'],
                    'clientOptions' => [
                        'create' => false,
                        'options' => Utility::makeReadyForSelectize(
                            Yii::$app->website->getUsersByPermission(['page.create', 'page.update']),
                            'email',
                            'email'
                        ),
                        'items' => $model->getUsersAsArray(),
                        'valueField' => 'email',
                        'labelField' => 'email',
                        'searchField' => ['email'],
                        'plugins' => ['remove_button'],
                    ],
                ]
                );
            ?>
        </div>
    <?php endif ?>

    <?php if (!empty($model->content) and !empty($model->link)): ?>
        <?= $form->field($model, 'link')->textInput(['class' => 'form-control input-large' ,'style' => 'direction:ltr;']) ?>
        <br>
        <br>
        <?= $form->field($model, 'content')->widget(Editor::className()); ?>
        <?php elseif (!empty($model->link)): ?>
        <?= Html::checkbox('setContent', false,['id' => 'setContent']) . ' Remove Url ' ?>
        <div id="link">

        <?= $form->field($model, 'link')->textInput([
            'class' => 'form-control input-large' ,
            'id' => 'linkInput',
            'style' => 'direction:ltr;'
        ]) ?>
        </div>
        <br>
        <br>
        <div id="content" hidden="hidden">
        <?= $form->field($model, 'content')->widget(
            Editor::className(),
            ['options' => ['id' => 'contentInput']]
        );?>
        </div>
    <?php else: ?>
        <?= Html::checkbox('setLink', false,['id' => 'setLink']).
            Module::t(' Insert Url ') ?>
        <div id="link" hidden="hidden">
        <?= $form->field($model, 'link')->textInput([
            'class' => 'form-control input-large' ,
            'style' => 'direction:ltr;',
            'id' => 'linkInput',
            'placeholder' => 'http://example.com'
        ]) ?>
        </div>
        <br>
        <br>
        <div id="content">
        <?= $form->field($model, 'content')->widget(Editor::className(),['options' => ['id' => 'contentInput']
        ]);?>
        </div>
    <?php endif ?>
    <?php Box::end() ?>
    <div class="form-group">
        <?= Html::submitButton(
            '<i class="fa fa-save"></i> '.Yii::t('core', 'Save'), [
            'class' => 'btn btn-lg btn-flat margin bg-green'
        ])?>
        <?= Button::widget([
            'title' => Yii::t('core', 'Cancel'),
            'options' => ['class' => 'btn-lg btn-flat margin'],
            'color' => 'orange',
            'icon' => 'undo',
            'url' => $backLink,
        ])
        ?>
    </div>
    </div>
    <div class="col-md-4">
        <?php Box::begin([
            'title' => Yii::t('core', 'Featured Image'),
            'options' => ['class' => 'box-primary'],
        ]) ?>
            <?php
                echo SingleImageUpload::widget(
                    [
                        'model' => $model,
                        'group' => 'image',
                    ]
                );
            ?>
        <?php Box::end() ?>
        <?php Box::begin([
            'title' => Yii::t('core', 'File'),
            'options' => ['class' => 'box-primary'],
        ]) ?>
        <div class="well">
            <ul>
                <li>
                    <?=Module::t('Valid Formats: pdf, mp4, ogg, flv, webm, zip, rar and mp3') ?>
                </li>
            </ul>
        </div>
        <?= SingleFileUpload::widget(
            [
                'model' => $model,
                'group' => 'file',
            ]
        );
        ?>
        <?php Box::end() ?>
        <?php Box::begin([
                    'title' => Yii::t('core', 'Tags'),
                    'options' => ['class' => 'box-primary'],
                ])?>
                <?=
                $form->field($model, 'tags')->widget(Select2::classname(), [
                    'name' => 'tags',
                    'options' => [
                        'id' => 'page-tags',
                        'placeholder' => Yii::t('core', 'Please Enter Tags ...')
                    ],
                    'pluginOptions' => [
                        'tags' => true,
                        'multiple' => true,
                        'minimumInputLength' => 2,
                        'language' => Yii::$app->language,
                        'ajax' => [
                            'url' => Url::to(['/tag/manage/find-tag']),
                            'dataType' => 'json',
                        ]
                    ],
                ]);
            ?>
            <?php Box::end() ?>
        <?php Box::begin([
            'title' => Module::t('More Information'),
            'options' => ['class' => 'box-primary']
            ]) ?>
                <?= $form->field($model, 'priority')->widget(Priority::classname());?>
                <?= $form->field($model, 'layout')
                    ->dropDownList(
                        Page::layoutLabel(),
                        ['class' => 'form-control input-medium']
                    )
                ?>
                <?= $form->field($model, 'isActive')->checkbox(); ?>
        <?php Box::end() ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs("
    $(document).ready(function(){
    $('#setLink').change(function(){
        if(this.checked){
            $('#link').fadeIn('slow');
            $('#content').slideUp('slow');
        }
        else{
            $('#link').fadeOut();
            $('#content').slideDown('slow');
            $('#linkInput').val('');
        }
    });
    $('#setContent').change(function(){
        if(this.checked){
            $('#link').slideUp('slow');
            $('#content').fadeIn('slow');
            $('#linkInput').val('');
        }
        else{
            $('#content').fadeOut();
            $('#link').slideDown('slow');
        }
    });

    $('#page-parentid').change(function(){
            if ( $('#page-parentid').val() > 0)
                $('#accesslevel').fadeOut();
            else
                 $('#accesslevel').fadeIn();
        })
});
", \yii\web\View::POS_READY, 'my-options');
