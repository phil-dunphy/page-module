<?php

use aca\common\helpers\Html;
use aca\page\backend\Module;
use aca\backend\widgets\actionButtons\ActionButtons;

$this->title = Yii::t('core', 'Edit'). ' " ' . $model->title . ' " ';
$this->params['breadcrumbs'][] = [
    'label' => Module::t('Pages'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">
    <p>
        <?= ActionButtons::widget([
            'buttons' => [
                'index' => [
                    'visibleFor'=>[
                        'page.create',
                        'page.update',
                        'page.delete',
                    ]
                ],
                'create' => [
                    'title' => Yii::t('core', 'Add New'),
                    'visibleFor'=>['page.create']
                ],
            ],
            'modelID' => $model->id,
        ]); ?>
    </p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
