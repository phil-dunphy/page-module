<?php

use aca\page\backend\Module;

$this->params['breadcrumbs'][] = ['label' => Module::t('Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->title,
    'url' => ['view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = Module::t('Gallery');
$this->title = Module::t('Gallery');

echo $this->render(
    '@aca/gallery/views/index.php',
    [
        'gallery' => $gallery,
        'ownerId' => $model->id
    ]
);
