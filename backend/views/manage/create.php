<?php

use aca\page\backend\Module;
use aca\common\helpers\Html;
use aca\backend\widgets\actionButtons\ActionButtons;

$this->title = Yii::t('core', 'Add New');
$this->params['breadcrumbs'][] = [
    'label' => Module::t('Pages'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-create">
    <?= ActionButtons::widget([
        'buttons' => [
            'index' => [
                'title' => Module::t('Pages'),
                'visibleFor'=>[
                    'page.create',
                    'page.update',
                    'page.delete',
                ]
            ],
        ],
    ]); ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
