<?php

use yii\bootstrap\Alert;
use yii\widgets\DetailView;
use aca\common\helpers\Html;
use aca\page\backend\Module;
use aca\backend\widgets\box\Box;
use aca\page\backend\models\Page;
use aca\backend\widgets\actionButtons\ActionButtons;

$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => Module::t('Pages'),
    'url' => ['index']
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">
    <p>
        <?= ActionButtons::widget([
            'model' => $model,
            'buttons' => [
                'index' => [
                    'title' => Module::t('Pages'),
                    'visibleFor' => [
                        'page.create',
                        'page.update',
                        'page.delete',
                    ],
                ],
                'create' => [
                    'title' => Yii::t('core', 'Add New'),
                    'visibleFor' => ['page.create']
                ],
                'delete' => [
                    'title' => Yii::t('core', 'Delete'),
                    'visibleFor' => ['page.delete']
                ],
                'update' => [
                    'title' => Yii::t('core', 'Edit'),
                    'visibleFor' => ['page.update']
                ],
                'gallery' => [
                    'title'=> $model->hasGallery() ? Yii::t('core', 'Manage Gallery') : Yii::t('core', 'Create Gallery'),
                    'visibleFor' => ['page.gallery']
                ],
                'preview' => [
                    'visibleFor' => ['page.create', 'page.update', 'page.delete'],
                ]
            ],
        ]); ?>
    </p>
    <p>
        <?php Alert::begin(['options' => ['class' => 'alert-warning'], 'closeButton' => false]); ?>
            <p>
                <b><?=Module::t('Warning')  ?> !</b>
                <?= Module::t('By deleting this page, all subset pages are also deleted. Currently')
                ?>
                <strong>* <?= Yii::$app->formatter->translateNumber($model->numberOfChildren) ?> *</strong>
                <?=Module::t('subset exists')  ?>
            </p>
        <?php Alert::end() ?>
     </p>
    <div class="row">
        <div class="col-md-6">
            <?php Box::begin([
                'title' => Yii::t('core', 'Basic Information'),
            ]) ?>
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id:translateNumber',
                    'title',
                    'priority:translateNumber',
                    'createdAt:date',
                    'updatedAt:date',
                    'isActive:boolean',
                    [
                        'attribute' => 'layout',
                        'value' => $model->getLayoutLabel()
                    ],
                    [
                        'attribute' => 'link',
                        'value' => Html::a(
                            $model->link,
                            $model->link,
                            ['target'=>'_blank']
                        ),
                        'format' => 'raw'
                    ],
                    [
                        'label' => Yii::t('core', 'Tags'),
                        'value' =>$model->getTagsAsString(),
                    ],
                    [
                        'label' => Module::t('Parent'),
                        'value' => isset($parent) ? he($parent->title) : "* ".Module::t('Page is Root')," *",
                        'format' => 'translateNumber',
                    ],
                    [
                        'attribute' => Yii::t('core', 'File'),
                        'value' => ($model->hasFile('file'))
                            ? Html::a($model->getFile('file')->name, $model->getFile('file')->url) : null,
                        'format' => 'raw'
                    ],
                    [
                        'label' => Yii::t('core', 'Visit Count'),
                        'value' => $model->getVisitStatistic(),
                        'format' => 'translateNumber'
                    ]
                ],
            ]) ?>
            <?php Box::end() ?>
        </div>
        <div class="col-md-6">
            <?php if (!empty($model->getFile('image'))) : ?>
                <?php Box::begin([
                        'title' => Yii::t('core', 'Featured Image'),
                        'options' => [
                            'class' => 'box-solid box-primary',
                        ],
                    ]);
                ?>
                <div class="row">
                    <div class="col-md-6">
                        <?= Html::img(
                            $model->getFile('image')->getUrl('view-thumb')
                        );
                        ?>
                    </div>
                </div>
                <?php Box::end() ?>
            <?php endif ?>
            <?php Box::begin([
                'title' => Module::t('Subsets'),
            ]) ?>
                <?php if (!empty($children)) : ?>
                    <div class="well">
                        <ul class="children" style="list-style:none; font-size:115%">
                            <li><?php echo he($model->title).' :' ?></li>
                            <?php foreach ($children as $child) : ?>
                                <li><?= he($child->nestedTitle) ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php else : ?>
                    <?=Module::t('This page has no subset')  ?>
                <?php endif ?>
            <?php Box::end() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php Box::begin([
                'title' => Module::t('Content'),
                'options' => ['class' => 'box-solid box-primary'],
                ]) ?>
                <div class="well">
                    <?= purify($model->content) ?>
                </div>
            <?php Box::end() ?>
        </div>
    </div>
</div>
