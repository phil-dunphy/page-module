<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use aca\common\helpers\Html;
use aca\page\backend\Module;
use aca\backend\widgets\box\Box;
use aca\page\backend\models\Page;
use aca\backend\widgets\actionButtons\ActionButtons;

$this->title = Module::t('Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Pages-index">
<div class="row">
    <div class="col-md-5">
        <p>
        <?= ActionButtons::widget([
                'buttons' => [
                    'create' => [
                        'title' => Yii::t('core', 'Add New'),
                        'visibleFor' => ['page.create']
                    ],
                ],
            ]); ?>
        </p>
    </div>
</div>

<?php Box::begin([
    'title' =>  Module::t('Pages'),
    'options' => ['class' => 'box-solid box-primary'],
    ]) ?>
<?php Pjax::begin([
    'id' => 'gridviewpjax',
    'enablePushState' => false,
    ]); ?>

    <?= GridView::widget([
        'pager' => [
            'firstPageLabel' => Yii::t('core', 'First'),
            'lastPageLabel'  => Yii::t('core', 'Last'),
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'aca\backend\grid\IDColumn'],
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    return he($model->nestedTitle);
                },
            ],
            [
                'class' => 'aca\backend\grid\ThumbnailColumn',
                'group' => 'image',
                'label' => Yii::t('core', 'Featured Image'),
            ],
            [
                'attribute' => 'createdAt',
                'format' => 'date',
                'filter' => false
            ],
            [
                'attribute' => 'priority',
                'format' => 'translateNumber',
            ],
            [
                'attribute' => 'layout',
                'value' => function ($model) {
                    return $model->getLayoutLabel();
                },
                'filter' => Page::layoutLabel()
            ],
            ['class' => 'aca\backend\grid\VisitColumn'],
            ['class' => 'aca\backend\grid\ActiveColumn'],
            ['class' => 'aca\backend\grid\ConfirmColumn'],
            [
                'class' => 'aca\backend\grid\ActionColumn',
                'template' => '{view} {update} {gallery} {preview}',
                'buttons' => [
                    'gallery' => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="fa fa-camera-retro"></span>',
                            $url,
                            [
                                'title' => Module::t('Gallery'),
                                'data-pjax' => 0
                            ]
                        );
                    },
                ],
                'permissions'=>[
                    'delete' => ['page.delete'],
                    'update' => ['page.update'],
                    'gallery' => ['page.gallery'],
                    'view' => ['page.create', 'page.update', 'page.delete', 'page.gallery']
                ]
            ],
        ],
    ]); ?>

<?php Pjax::end(); ?>
<?php Box::end() ?>
</div>
