<?php

use aca\common\helpers\Html;
use aca\page\backend\Module;
use yii\bootstrap\ActiveForm;
use aca\backend\widgets\box\Box;
use aca\backend\widgets\button\Button;
use aca\backend\widgets\activeWebsiteView\ActiveWebsiteView;

$this->title = Module::t('Page Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="setting-form">
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'setting-form']); ?>
            <?php Box::begin([
                'title' => Yii::t('core', 'Settings'),
                'options' => ['class' => 'box-primary'],
            ]) ?>
            <?= ActiveWebsiteView::widget(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field(
                        $settings['page.hasAccessLevel'],
                        "[page.hasAccessLevel]value"
                    )->checkbox()->label(Module::t('Add Access Level To Page'));
                    ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field(
                        $settings['page.lastUpdatedAt'],
                        "[page.lastUpdatedAt]value"
                    )->checkbox()->label(Module::t('Show Last Edited Time'));
                    ?>
                </div>
                <div class="col-md-4">
                    <?php echo $form->field(
                        $settings['page.showVisitStatistic'],
                        "[page.showVisitStatistic]value"
                    )->checkbox()->label(Module::t('Show Visit Count'));
                    ?>
                </div>
            </div>
            <div class="row">
                <?php if (Yii::$app->setting->get('website.editorIsEnabled')) : ?>
                    <div class="col-md-4">
                        <?= $form->field(
                            $settings['page.editor'],
                            "[page.editor]value"
                        )
                        ->checkbox()
                        ->label(Yii::t('core', 'Confirm Status'));
                        ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php Box::end() ?>
            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-save"></i> '.Yii::t('core', 'Save'), [
                    'class' => 'btn btn-lg btn-flat margin bg-green',
                ]) ?>
                <?= Button::widget([
                    'title' => Yii::t('core', 'Cancel'),
                    'options' => ['class' => 'btn-lg btn-flat margin'],
                    'color' => 'orange',
                    'icon' => 'undo',
                    'url' => ['index'],
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
