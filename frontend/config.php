<?php
return [
    'urlRules' => [
        'page/popular' => 'page/front/popular',
        'page/preview/<id>/<title>' => 'page/front/preview'
    ],
    'components' => [
        // list of component configurations
    ],
    'params' => [
        // list of parameters
    ],
];
