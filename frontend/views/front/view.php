<?php
use aca\common\helpers\Html;
use aca\gallery\widgets\gallery\Gallery;
use aca\frontend\widgets\fileView\FileView;
use aca\frontend\widgets\toolButtons\ToolButtons;
use aca\page\frontend\widgets\pagefamily\PageFamily;
use aca\frontend\widgets\showLastUpdate\ShowLastUpdate;
use aca\tag\frontend\widgets\showTags\ShowTags;

$this->params['breadcrumbs'] = $page->getBreadCrumbsItems();

?>
<div class="page-view article-view">
    <article>
        <h1><?php echo he($page->title) ?></h1>
        <?php if (!empty($page->getFile('image'))) : ?>
            <figure>
                <?= Html::img($page->getFile('image')->getUrl('page-image'), [
                    "class" => "img-responsive",
                    "alt" => he($page->title),
                    "preset" => "page-image"
                ]); ?>
            </figure>
        <?php endif; ?>
        <div class="content">
            <?= purify($page->content) ?>
        </div>
    </article>
    <?= FileView::widget([
        'model' => $page,
        'fileName' => 'file'
    ])
    ?>
    <div style="clear:both;"></div>
    <?= Gallery::widget(
        [
            'images'=> $page->getGalleryImages(),
                'clientOptions' => [
                'nav'     => true,
                'navText' => [
                    '<i class="fa fa-chevron-left"></i>',
                    '<i class="fa fa-chevron-right"></i>'
                ],
                'dots'    => true,
            ]
        ]
    ) ?>

    <?= ShowTags::widget(['model' => $page]) ?>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <?php
                    echo ShowLastUpdate::widget([
                        'model' => $page,
                        'visible' => Yii::$app->setting->getModuleSetting('page.lastUpdatedAt')
                    ]);
                ?>
            </div>
            <div class="pull-left">
                <?php echo ToolButtons::widget([
                    'buttons' => [
                        'visit' => [
                            'visitCount' => Yii::$app->formatter->translateNumber($page->getVisitStatistic()),
                            'visibleFor' => Yii::$app->setting->getModuleSetting('page.showVisitStatistic')
                        ]
                    ]
                ]);?>
            </div>
        </div>
    </div>
</div>
<?php $this->beginBlock('sidebar');?>
    <?= PageFamily::widget([
            'page' => $page,
            'listClass' => 'list-unstyled',
            'containerClass' => 'side-menu',
        ]) ?>
<?php $this->endBlock() ?>
