<?php
use aca\page\frontend\Module;
use aca\page\frontend\widgets\mostVisited\MostVisitedPages;

$this->title = Module::t('Most Visited Pages');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-view article-view">
    <article>
        <h1><?php echo he($this->title) ?></h1>
    </article>
    <div class="row">
        <div class="col-md-12">
            <?= MostVisitedPages::widget([
                'listClass' => 'list-unstyled',
                'containerClass' => 'content',
                'view' => 'page',
            ]); ?>
        </div>
    </div>
</div>
