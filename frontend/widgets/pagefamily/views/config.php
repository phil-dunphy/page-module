<?php
use yii\bootstrap\Html;
use aca\page\frontend\Module;
use aca\page\frontend\widgets\pageFamily\PageFamily;

?>
<h2 class="module-title"><?= Module::t('Pages') ?></h2>
<?= Html::beginForm('', 'post', ['class'=>"form-horizontal"]); ?>
    <?= Html::hiddenInput('id', $widget->id); ?>
    <div class="form-group">
        <div class="col-sm-4">
            <?= Html::label(PageFamily::t('Title')); ?>
        </div>
        <div class="col-sm-5">
            <?= Html::textInput(
                'params[title]',
                $params['title'],
                ['class'=>"form-control input-small"]
            );?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <?= Html::label(PageFamily::t('Show Title')); ?>
        </div>
        <div class="col-sm-1">
           <?= Html::checkbox(
                'params[showTitle]',
                $params['showTitle'],
                [
                    'uncheck' => '0',
                    'class'=>"form-control input-small"
                ]
            ); ?>
        </div>
    </div>

<?= Html::endForm() ?>
