<?php
    use aca\common\helpers\Html;

?>
<div class="nestedpage-widget widget <?= $this->context->containerClass ?>">

    <?php if ($this->context->showTitle): ?>
        <h4 class="<?= $this->context->titleClass ?>">
            <?php if (!empty($this->context->titleIcon)) {
                echo $this->context->titleIcon;
            } ?>
            <?php echo $this->context->title; ?>
        </h4>
    <?php endif ?>

    <ul class="<?= $this->context->listClass ?>">
        <?php if (!$page->isRoot() && !empty($parent)) : ?>
            <li class="parent">
                <?php echo Html::a($this->context->listIcon.' '.he($parent->title), [
                    '/page/front/view','id' => $parent->id , 'title' => he($parent->title)
                ]);
                ?>
            </li>
        <?php elseif ($page->isRoot() or empty($parent)): ?>
            <li class="active parent">
                <?php echo $this->context->listIcon.' '.he($page->title); ?>
            </li>
        <?php endif ?>
        <?php
        $printArray = ($page->isRoot() or empty($parent)) ? $children : $sibling  ;
        ?>
        <?php foreach ($printArray as $node): ?>
            <?php if ($node->id == $page->id): ?>
                <li class="active">
                    <?= $this->context->listIcon.' '.he($node->title) ?>
                </li>
                    <?php if (isset($children)): ?>
                        <ul class="<?= $this->context->listClass ?>">
                            <?php foreach ($children as $child): ?>
                                <li>
                                <?= Html::a(
                                    $this->context->listIcon.' '.he($child->title),
                                    [
                                        '/page/front/view',
                                        'id' => $child->id,
                                        'title' => he($child->title)
                                    ]
                                ) ?>
                                </li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif ?>
            <?php else: ?>
                <li>
                <?= Html::a(
                    $this->context->listIcon.' '.he($node->title),
                    [
                        '/page/front/view',
                        'id' => $node->id,
                        'title' => he($node->title)
                    ]
                ); ?>
                </li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
</div>
