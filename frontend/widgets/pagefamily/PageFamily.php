<?php
namespace aca\page\frontend\widgets\pagefamily;

use Yii;
use yii\base\Widget;
use aca\common\helpers\Html;
use aca\page\frontend\Module;
use aca\widgetConfig\classes\ConfigurableWidget;

class PageFamily extends ConfigurableWidget
{
    public $page;
    public $view = 'pageFamily';
    public $title;
    public $titleIcon;
    public $containerClass;
    public $titleClass;
    public $listClass;
    public $listIcon;
    public $showTitle = true;

    protected $configurableProperties = ['title', 'showTitle'];

    public function init()
    {
        parent::init();
        $this->registerTranslations();
        $this->titleIcon = (isset($this->titleIcon)) ? '<i class="fa fa-'.$this->titleIcon.'"></i>' : null ;
        $this->listIcon = (isset($this->listIcon)) ? '<i class="fa fa-'.$this->listIcon.'"></i>' : "-" ;
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['@aca/page/frontend/widgets/pageFamily/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@aca/page/common/messages',
            'fileMap' => [
                '@aca/page/frontend/widgets/pageFamily/messages' => 'widget.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        if (!Yii::$app->setting->get('website.translateBackend')) {
            $language = 'fa';
        }
        return \Yii::t('@aca/page/frontend/widgets/pageFamily/messages', $message, $params, $language);
    }

    public function run()
    {
        if (!isset($this->title)) {
            $this->title = Module::t('Related Pages');
        }
        $sibling =[];
        $parent=[];
        $children = $this->page->children(1)->orderBy(['priority' => SORT_DESC, 'createdAt' => SORT_DESC])->all();
        if (!$this->page->isRoot()) {
            $parent = $this->page->parents(1)->one();
            if (!empty($parent)) {
                $sibling = $parent->children(1)->orderBy(['priority' => SORT_DESC, 'createdAt' => SORT_DESC])->all();
            }
        }
        return $this->render(
            $this->view,
            [
                'page' => $this->page,
                'parent' => $parent,
                'children' => $children,
                'sibling' => $sibling,
            ]
        );
    }
}
