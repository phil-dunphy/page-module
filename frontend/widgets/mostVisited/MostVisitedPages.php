<?php
namespace aca\page\frontend\widgets\mostVisited;

use Yii;
use aca\page\frontend\Module;
use yii\caching\TagDependency;
use aca\page\frontend\models\Page;
use aca\widgetConfig\classes\ConfigurableWidget;

class MostVisitedPages extends ConfigurableWidget
{
    public $title;
    public $listIcon;
    public $titleIcon;
    public $listClass;
    public $titleClass;
    public $view = 'widget'; //this view is in ul li format, page view use p tag as element container
    public $containerClass;
    public $showTitle = true;
    public $numberOfPages = 5;

    protected $configurableProperties = ['title', 'showTitle', 'numberOfPages'];

    public function init()
    {
        parent::init();
        $this->registerTranslations();
        $this->titleIcon = (isset($this->titleIcon)) ? '<i class="fa fa-'.$this->titleIcon.'"></i>' : null ;
        $this->listIcon = (isset($this->listIcon)) ? '<i class="fa fa-'.$this->listIcon.'"></i>' : "-" ;
    }

    public function registerTranslations()
    {
        $i18n = Yii::$app->i18n;
        $i18n->translations['@aca/page/frontend/widgets/mostVisited/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@aca/page/common/messages',
            'fileMap' => [
                '@aca/page/frontend/widgets/mostVisited/messages' => 'widget.php',
            ],
        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        if (!Yii::$app->setting->get('website.translateBackend')) {
            $language = 'fa';
        }
        return \Yii::t('@aca/page/frontend/widgets/mostVisited/messages', $message, $params, $language);
    }

    public function run()
    {
        if (empty($this->title)) {
            $this->title = Module::t('Most Visited');
        }
        $pages = Page::getDb()->cache(function ($db) {
            return Page::find()
            ->select(['id', 'title', 'visitCounter'])
            ->limit($this->numberOfPages)
            ->orderBy('visitCounter DESC')
            ->all();
        }, 60*25, $this->getCacheKey());
        return $this->render($this->view, ['pages' => $pages]);
    }

    public function getCacheKey()
    {
        return new TagDependency(
            ['tags' => 'Page-'.Yii::$app->website->id]
        );
    }
}
