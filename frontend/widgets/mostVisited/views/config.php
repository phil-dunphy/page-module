<?php
use yii\bootstrap\Html;
use aca\page\frontend\Module;
use aca\common\helpers\Utility;
use aca\page\frontend\widgets\mostVisited\MostVisitedPages;

?>
<h2 class="module-title"><?= Module::t('Pages') ?></h2>
<?= Html::beginForm('', 'post', ['class'=>"form-horizontal"]); ?>
    <?= Html::hiddenInput('id', $widget->id); ?>
    <div class="form-group">
        <div class="col-sm-4">
            <?= Html::label(MostVisitedPages::t('Show Title')); ?>
        </div>
        <div class="col-sm-1">
            <?= Html::checkbox('params[showTitle]', $params['showTitle'], [
                'uncheck' => '0',
                'class'=>"form-control input-small"
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <?= Html::label(MostVisitedPages::t('Title')); ?>
        </div>
        <div class="col-sm-5">
            <?= Html::textInput('params[title]', $params['title'], [
                'class'=>"form-control input-small"
            ]);?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-4">
            <?= Html::label(MostVisitedPages::t('Page Count')); ?>
        </div>
        <div class="col-sm-5">
            <?= Html::dropDownList('params[numberOfPages]', $params['numberOfPages'], Utility::listNumbers(5, 20), [
                'class'=>"form-control input-small"
            ]);?>
        </div>
    </div>
<?= Html::endForm() ?>
