<?php
    use aca\common\helpers\Html;

?>
<div class="most-visited-page-widget widget <?= $this->context->containerClass ?>">
    <?php if ($this->context->showTitle) : ?>
        <h4 class="<?= $this->context->titleClass ?>">
            <?php if (!empty($this->context->titleIcon)) {
                echo $this->context->titleIcon;
            }?>
            <?php echo $this->context->title; ?>
        </h4>
    <?php endif ?>
    <ul class="<?= $this->context->listClass ?>">
        <?php foreach ($pages as $page) : ?>
            <li>
                <?php echo Html::a($this->context->listIcon.' '.he($page->title), [
                    '/page/front/view','id' => $page->id , 'title' => he($page->title)
                ]);
                ?>
            </li>
        <?php endforeach ?>
    </ul>
</div>
