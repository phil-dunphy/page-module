<?php

namespace aca\page\frontend;


use yii\data\ActiveDataProvider;
use aca\page\frontend\models\Page;
use yii\helpers\BaseStringHelper;
use Yii;

class Module extends \yii\base\Module
{
    public $urlRules;
    public $controllerNamespace = 'aca\page\frontend\controllers';

    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        \Yii::$app->i18n->translations['modules/page/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@aca/page/common/messages',
            'fileMap' => [
                'modules/page/module' => 'module.php',
            ],

        ];
    }

    public static function t($message, $params = [], $language = null)
    {
        return \Yii::t('modules/page/module', $message, $params, $language);
    }

    public function loadDataForTagModule($ids)
    {
        return new ActiveDataProvider(
            [
                'query' => Page::find()->andWhere(['in', 'id',$ids]),
                'pagination' => [
                    'pageSize' => 30,
                ],
            ]
        );
    }

    public function loadDataForRss($object)
    {
        $data['title'] = $object->title;
        $data['id'] = $object->id;
        $data['summary'] = BaseStringHelper::truncateWords(strip_tags($object->content),100);
        $data['publishedAt'] = $object->createdAt;
        $data['link'] = urldecode(Yii::$app->urlManager->createAbsoluteUrl([
                                '/page/front/view',
                                'id' => $object->id,
                                'title' => $object->title,
                            ]));
        $data['image'] = ($object->getFile('image')) ?
                            Yii::$app->urlManager->createAbsoluteUrl([
                                $object->getFile('image')->getUrl('rss')
                                ]) :
                            null;

        return $data;
    }

}
