<?php
namespace aca\page\frontend\models;

use Yii;
use yii\db\ActiveQuery;
use yii\Helpers\ArrayHelper;
use aca\visit\behaviors\VisitBehavior;
use aca\frontend\behaviors\SeoBehavior;
use aca\gallery\behaviors\GalleryBehavior;
use aca\fileManager\behaviors\FileBehavior;
use aca\tag\common\behaviors\TaggableBehavior;

class Page extends \aca\page\common\models\Page
{
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                GalleryBehavior::className(),
                [
                    'class' => FileBehavior::className(),
                    'groups' => [
                        'image' => [
                            'type' => FileBehavior::TYPE_IMAGE,
                        ],
                    ],
                ],
                'tag' => [
                    'class' => TaggableBehavior::className(),
                    'moduleId' => 'page',
                ],
                [
                    'class' => SeoBehavior::className(),
                    'urlRoute' => '/page/front/view',
                    'urlParams' => ['id' => $this->id, 'title' => $this->title]
                ],
                VisitBehavior::className(),
            ]
        );
    }

    public static function find()
    {
        $query = new PageQuery(get_called_class());
        $query = $query->where(
            'page.websiteId = :id',
            [':id' => Yii::$app->website->id]
        );
        if (Yii::$app->controller->action->id != 'preview') {
            $query = $query->andWhere('isActive = 1');
            if (
                    Yii::$app->setting->get('website.editorIsEnabled') and
                    Yii::$app->setting->getModuleSetting('page.editor')
                ) {
                $query = $query->andWhere('page.confirmStatus = 2');//2 means that editor accepted this data,
            }
        }
        return $query;
    }

    public function getBreadCrumbsItems()
    {
        $items = [];
        foreach ($this->parents()->all() as $parent) {
            $items[] = [
                'label' => $parent->title,
                'url' => [
                    '/page/front/view',
                    'id' => $parent->id,
                    'title' => $parent->title
                ]
            ];
        }
        return $all = ArrayHelper::merge($items, [$this->title]);
    }
}
