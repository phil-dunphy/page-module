<?php

namespace aca\page\frontend\controllers;

use yii\web\Controller;
use aca\page\frontend\models\Page;
use yii\web\NotFoundHttpException;
use aca\frontend\filters\SeoFilter;

class FrontController extends Controller
{
    public $layout = '//two-column';

    public function behaviors()
    {
        return [
            [
                'class' => SeoFilter::className(),
                'actions' => [
                    'view' => [
                        'className' => Page::className(),
                        'imageName' => 'image',
                    ],
                ]
            ],
            [
                'class' => 'aca\visit\filters\VisitFilter',
                'actions' => ['view' => Page::className()]
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['preview'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'preview' => [
                'class' => 'aca\frontend\actions\PreviewAction',
                'className' => Page::className(),
                'modelName' => 'page'
            ]
        ];
    }

    public function actionView($id)
    {
        $page = $this->findModel($id);
        $this->layout = ($page->layout == 1) ? '//one-column' : '//two-column' ;
        if (!empty($page->link)) {
            $this->redirect($page->link);
        }
        return $this->render('view', [
            'page' => $page,
        ]);
    }

    public function actionPopular()
    {
        return $this->render('popular');
    }

    protected function findModel($id)
    {
        if (($model = Page::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
